package geom.clipper;

/**
 * ...
 * @author vsugrob
 */

enum PolyKind {
	Subject;
	Clip;
}