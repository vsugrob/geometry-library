package geom.clipper;

/**
 * ...
 * @author vsugrob
 */

enum PolyFill {
	EvenOdd;
	NonZero;
}