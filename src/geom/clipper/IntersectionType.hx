package geom.clipper;

/**
 * ...
 * @author vsugrob
 */

enum IntersectionType {
	LeftIntermediate;
	RightIntermediate;
	LocalMaxima;
	LocalMinima;
}